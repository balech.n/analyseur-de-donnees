

import java.lang.reflect.*;
import java.util.*;

/**
  * TraitementBuilder 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class TraitementBuilder {

	/** Retourne un objet de type Class correspondant au nom en param�tre.
	 * Exemples :
	 *   - int donne int.class
	 *   - Normaliseur donne Normaliseur.class
	 */
	Class<?> analyserType(String nomType) throws ClassNotFoundException {
		switch(nomType) {
			case "int":
				return int.class;
			case "double":
				return double.class;
			case "float":
				return float.class;
			default:
				return Class.forName(nomType);
		}
		
	}

	/** Cr�e l'objet java qui correspond au type formel en exploitant le « mot » suivant du scanner.
	 * Exemple : si formel est int.class, le mot suivant doit �tre un entier et le r�sulat est l'entier correspondant.
	 * Ici, on peut se limiter aux types utlis�s dans le projet : int, double et String.
	 */
	static Object decoderEffectif(Class<?> formel, Scanner in) {
		if(formel == int.class) {
			return Integer.parseInt(in.next());
		}else if(formel == double.class) {
			return Double.parseDouble(in.next());
		}else if(formel == String.class) {
			return in.next();
		}
		return null;
        
	}
	


	/** Définition de la signature, les paramètres formels, mais aussi les paramètres formels.  */
	static class Signature {
		Class<?>[] formels;
		Object[] effectifs;

		public Signature(Class<?>[] formels, Object[] effectifs) {
			this.formels = formels;
			this.effectifs = effectifs;
		}
	}

	/** Analyser une signature pour retrouver les param�tres formels et les param�tres effectifs.
	 * Exemple « 3 double 0.0 String xyz int -5 » donne
	 *   - [double.class, String.class, int.class] pour les param�tres effectifs et
	 *   - [0.0, "xyz", -5] pour les param�tres formels.
	 */
	Signature analyserSignature(Scanner in) throws ClassNotFoundException {
		int nbParams = Integer.parseInt(in.next());
 
        Class<?>[] formels = new Class<?>[nbParams];
        Object[] effectifs = new Object[nbParams];
        
        for(int i = 0; i < nbParams; i++) {
            formels[i] = analyserType(in.next());
            effectifs[i] = decoderEffectif(formels[i], in);
        }
        return new Signature(formels,effectifs);
	}


	/** Analyser la création d'un objet.
	 * Exemple : « Normaliseur 2 double 0.0 double 100.0 » consiste à charger
	 * la classe Normaliseur, trouver le constructeur qui prend 2 double, et
	 * l'appeler en lui fournissant 0.0 et 100.0 comme paramètres effectifs.
	 */
	Object analyserCreation(Scanner in)
		throws ClassNotFoundException, InvocationTargetException,
						  IllegalAccessException, NoSuchMethodException,
						  InstantiationException
	{
		Class<?> o = analyserType(in.next());
		Signature signature = analyserSignature(in);
		Constructor<?> constructor = o.getConstructor(signature.formels);
		return constructor.newInstance(signature.effectifs);
	}


	/** Analyser un traitement.
	 * Exemples :
	 *   - « Somme 0 0 »
	 *   - « SupprimerPlusGrand 1 double 99.99 0 »
	 *   - « Somme 0 1 Max 0 0 »
	 *   - « Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 0 »
	 *   - « Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 1 Positions 0 0 »
	 * @param in le scanner à utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 */
	Traitement analyserTraitement(Scanner in, Map<String, Traitement> env)
		throws ClassNotFoundException, InvocationTargetException,
						  IllegalAccessException, NoSuchMethodException,
						  InstantiationException
	{
		ArrayList<Traitement> traitementsSuivants = new ArrayList<>();
		Traitement traitementInitial = (Traitement) analyserCreation(in);
		int nbTraitementsRestants = Integer.parseInt(in.next());
		
		for(int i = 0; i < nbTraitementsRestants;i++) {
			traitementsSuivants.add(i,analyserTraitement(in,env));
		}
		Method ajouterSuivants = traitementInitial.getClass().getMethod("ajouterSuivants", Traitement[].class);
		ajouterSuivants.invoke(traitementInitial, new Object[]{traitementsSuivants.toArray(new Traitement[0])});		
		return traitementInitial;
	}


	/** Analyser un traitement.
	 * @param in le scanner à utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 */
	public Traitement traitement(Scanner in, Map<String, Traitement> env)
	{
		try {
			return analyserTraitement(in, env);
		} catch (Exception e) {
			throw new RuntimeException("Erreur sur l'analyse du traitement, "
					+ "voir la cause ci-dessous", e);
		}
	}

}
