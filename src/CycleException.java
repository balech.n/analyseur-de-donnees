
public class CycleException extends RuntimeException{
	
	public CycleException() {
		super("Le traitement suivant se trouve d�j� dans le cycle");
	}
}
