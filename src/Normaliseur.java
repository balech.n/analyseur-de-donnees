import java.util.ArrayList;

/**
  * Normaliseur normalise les données d'un lot en utilisant une transformation affine.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Normaliseur extends Traitement {
	
	private double debut;
	private double fin;
	
	private Max max;
	private Max min;
	private Donnees donnees;
	
	public Normaliseur(double debut, double fin) {
		this.debut = debut;
		this.fin = fin;
	}
	
	@Override
	public void gererDebutLotLocal(String nomLot) {
		FabriqueTraitement traitement = new FabriqueTraitementConcrete();
		this.donnees = traitement.donnees();
		this.max = traitement.max();
		this.min = traitement.max();
		Multiplicateur multiplicateur = traitement.multiplicateur(-1);
		
		this.donnees.ajouterSuivants(max);
		max.ajouterSuivants(multiplicateur);
		multiplicateur.ajouterSuivants(min);
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		ArrayList<Position> positions = this.donnees.getPositions();
		ArrayList<Double> valeurs = this.donnees.getValeurs();
		Double a = (this.max.getMax() - this.min.getMax() *(-1))/ (this.fin - this.debut);
		Double b = this.debut - (a * this.min.getMax());
		for(int i = 0; i < positions.size(); i++) {
			Double x = a * valeurs.get(i) + b;
			super.traiter(positions.get(i), x);
		}
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.donnees.traiter(position, valeur);
	}
	
	

}