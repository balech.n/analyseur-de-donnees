

/**
  * Max calcule le max des valeurs vues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Max extends Traitement {

	private double max;
	
	public Max() {
		this.max = 0;
	}
	
	public double getMax() {
		return max;
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": max = " + this.max);
	}
	
	public double max() {
		return this.max;
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		if(valeur > this.max)
			this.max = valeur;
		
		super.traiter(position, valeur);
	}
}
