import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * Maj indique pour chaque lot les positions mises à jour (ou ajoutées)
 * lors du traitement de ce lot.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Maj extends Traitement {

	private ArrayList<Position> positions = new ArrayList<>();
	private TreeMap<String, ArrayList<Position>> lots = new TreeMap<>();
	
	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.positions = new ArrayList<>();
		if(!this.lots.containsKey(nomLot))
			this.lots.put(nomLot, new ArrayList<>());
		
		this.lots.replace(nomLot, this.positions);

	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		Iterator<?> iterator = this.lots.entrySet().iterator();
		while(iterator.hasNext()) {
	        Map.Entry<String,ArrayList<Position>> entry = (Entry<String, ArrayList<Position>>)iterator.next();
			System.out.println("Maj lot " + entry.getKey() +": ");
			for(Position pos : entry.getValue()) {
				System.out.println("\t - Position(" + pos.x + "," + pos.y + ")");
			}
			System.out.println("Fin Maj lot " + entry.getKey());
		}
		System.out.println();
		
	}

	
	@Override
	public void traiter(Position position,double valeur) {
		if(!this.positions.contains(position))
			this.positions.add(position);
		super.traiter(position, valeur);
	}

}
