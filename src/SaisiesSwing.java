import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SaisiesSwing implements ActionListener{
	
	private JFrame fenetre;
	private JPanel pan;
	private File fichier;
	private FileWriter writer;
	private JTextField abscisse, ordonnee,valeur;
	private String donnees = "";
	private int numeroOrdre = 1;
	private boolean absOk = false, ordOk=false, valeurOk=false;
	
	public SaisiesSwing(String nomFichier) {
		try {
			this.fichier = new File("Fichiers de donn�es/" + nomFichier);
			this.fichier.createNewFile();
			this.writer = new FileWriter(fichier);
		} catch (IOException e) {
			System.out.println("Impossible de cr�er le fichier");
		}
		this.fenetre = new JFrame();
		this.pan = new JPanel();
		
		this.fenetre.setTitle("Saisie donn�es");
		this.fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.fenetre.setLocationRelativeTo(null);
		this.fenetre.setSize(400, 200);
		this.fenetre.setResizable(false);
		this.fenetre.setContentPane(this.pan);
		this.fenetre.setVisible(true);
		
		JButton valider = new JButton("Valider");
		valider.addActionListener(this);
		JButton effacer = new JButton("Effacer");
		effacer.addActionListener(this);
		JButton terminer = new JButton("Terminer");
		terminer.addActionListener(this);
		
		JLabel abscisseLabel = new JLabel("Abscisse :", JLabel.RIGHT);
		abscisseLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 3));
		JLabel ordonneeLabel = new JLabel("Ordonn�e :", JLabel.RIGHT);
		ordonneeLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 3));
		JLabel valeurLabel = new JLabel("Valeur :", JLabel.RIGHT);
		valeurLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 3));
		
		this.abscisse = new JTextField();
		this.ordonnee = new JTextField();
		this.valeur = new JTextField();
		
		this.pan.setLayout(new BorderLayout());
		JPanel panNorth = new JPanel();
		JPanel panSouth = new JPanel();
		this.pan.add(panNorth, BorderLayout.NORTH);
		this.pan.add(panSouth, BorderLayout.SOUTH);
		
		panNorth.setLayout(new GridLayout(3,2));
		panSouth.setLayout(new GridLayout(1,3,5,0));
		
		panNorth.add(abscisseLabel);
		panNorth.add(abscisse);
		panNorth.add(ordonneeLabel);
		panNorth.add(ordonnee);
		panNorth.add(valeurLabel);
		panNorth.add(valeur);
		
		panSouth.add(valider);
		panSouth.add(effacer);
		panSouth.add(terminer);
		
		this.fenetre.pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton source = (JButton)e.getSource();

		if(source.getText().equals("Valider")){
			
			if(this.verifierValeurs()) {
				this.donnees += this.abscisse.getText()+ " " + this.ordonnee.getText() + " " + this.numeroOrdre + " " + this.valeur.getText() +"\n";
				this.numeroOrdre +=1;
				this.abscisse.setText("");
				this.abscisse.setBackground(Color.WHITE);
				this.ordonnee.setText("");
				this.ordonnee.setBackground(Color.WHITE);
				this.valeur.setText("");
				this.valeur.setBackground(Color.WHITE);
				this.absOk = false;
				this.ordOk = false;
				this.valeurOk = false;
			} else {
				this.verifierValeurs();
			}
		}else if(source.getText() == "Effacer") {
			
			this.abscisse.setText("");
			this.abscisse.setBackground(Color.WHITE);
			this.ordonnee.setText("");
			this.ordonnee.setBackground(Color.WHITE);
			this.valeur.setText("");
			this.valeur.setBackground(Color.WHITE);
			
		}else if(source.getText() == "Terminer") {
			try {
				this.writer.write(this.donnees);
				this.writer.close();
			} catch (IOException exception) {
				exception.printStackTrace();
			}
			this.fenetre.dispose();
		}
	}

	/**
	 * V�rifie qu'un input est bien un entier
	 * @param str
	 * @return boolean
	 */
	public boolean isInteger(String str) {
		try {
		   int i = Integer.parseInt(str);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * V�rifie qu'un input est bien un r�el
	 * @param str
	 * @return boolean
	 */
	public boolean isFloat(String str) {		
		try {
	        float f = Float.parseFloat(str);
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	    return true;
	}
	/**
	 * V�rifie si les valeurs entr�es correspondent aux types attendus
	 * @return boolean
	 */
	public boolean verifierValeurs() {
		String abs = this.abscisse.getText();
		String ord = this.ordonnee.getText();
		String val = this.valeur.getText();
		
		if(!this.isInteger(abs)){
			this.abscisse.setBackground(Color.RED);
		}else {
			this.abscisse.setBackground(Color.WHITE);
			this.absOk = true;
		}
		
		if(!this.isInteger(ord)){
			this.ordonnee.setBackground(Color.RED);
		}else {
			this.ordonnee.setBackground(Color.WHITE);
			this.ordOk = true;
		}
		
		if(!this.isFloat(val)){
			this.valeur.setBackground(Color.RED);
		}else {
			this.valeur.setBackground(Color.WHITE);
			this.valeurOk = true;
		}
		
		if(this.valeurOk && this.absOk && this.ordOk) {
			return true;
		}else {
			return false;
		}
	}
	
}
