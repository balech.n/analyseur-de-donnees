

import java.util.*;

/**
  * PositionsAbstrait spécifie un traitement qui mémorise
  * toutes les positions traitées pour ensuite y accéder
  * par leur indice ou obtenir la fréquence d'une position.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

abstract public class PositionsAbstrait extends Traitement {

	/** Obtenir le nombre de positions m�moris�es.
	 * @return le nombre de positions m�moris�es
	 */
	public abstract int nombre();

	/** Obtenir la i�me position enregistr�e.
	 * @param numero num�ro de la position souhait�e (0 pour la premi�re)
	 * @return la position de num�ro d'ordre `numero`
	 * @exception IndexOutOfBoundsException si le numero est incorrect
	 */
	public abstract Position position(int indice);

	/** Obtenir la fr�quence d'une position dans les positions trait�es.
	 * @param position la position dont on veut conna�tre la fr�quence
	 * @return la fr�quence de la position en param�tre
	 */
	public abstract int frequence(Position position);
	

}
