import java.util.*;

/**
  * Traitement mod�lise un traitement et ses traitements suivants (donc
  * une cha�ne de traitements).
  *
  * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
  */
abstract public class Traitement {

	/** Les traitements suivants. */
	private List<Traitement> suivants = new ArrayList<>();

	/** Ajouter des traitements � la suite de celui-ci.
	 * @param suivants les traitements � ajouter
	 */
	final public Traitement ajouterSuivants(Traitement... suivants) throws CycleException{
		try{
			Collections.addAll(this.suivants, suivants);
		}catch(CycleException e) {
			e.printStackTrace();
		}
		return this;
	}

	@Override
	public final String toString() {
		return this.toString("");
	}

	/** Afficher ce traitement et les suivants sous forme d'un arbre horizontal.
	 * @param prefixe le pr�fixe � afficher apr�s un retour � la ligne
	 */
	private String toString(String prefixe) {
		String res =  this.getClass().getName();
		String complement = this.toStringComplement();
		if (complement != null && complement.length() > 0) {
			res += "(" + complement + ")";
		}
		if (this.suivants.size() <= 1) {
			for (Traitement s : this.suivants) {
				res += " --> " + s.toString(prefixe);
			}
		} else {
			for (Traitement s : this.suivants) {
				res += "\n" + prefixe + "\t" + "--> " + s.toString(prefixe + "\t");
			}
		}
		return res;
	}

	/** D�crire les param�tres du traitement. */
	protected String toStringComplement() {
		return null;
	}

	public void traiter(Position position, double valeur) {
		for (Traitement suivant : this.suivants) {
			suivant.traiter(position, valeur);
		}
	}

	final public void gererDebutLot(String nomLot) {
		Objects.requireNonNull(nomLot, "nomLot doit �tre d�fini.");

		this.gererDebutLotLocal(nomLot);
		for (Traitement suivant : this.suivants) {
			suivant.gererDebutLot(nomLot);
		}
	}

	protected void gererDebutLotLocal(String nomLot) {
	}

	final public void gererFinLot(String nomLot) {
		Objects.requireNonNull(nomLot, "nomLot doit �tre d�fini.");

		this.gererFinLotLocal(nomLot);
		for (Traitement suivant : this.suivants) {
			suivant.gererFinLot(nomLot);
		}
	}

	protected void gererFinLotLocal(String nomLot) {
	}

}
