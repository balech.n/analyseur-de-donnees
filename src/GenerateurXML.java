import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

import org.jdom2.*;
import org.jdom2.output.*;

/**
 * GenerateurXML écrit dans un fichier, à charque fin de lot, toutes
 * les données lues en indiquant le lot dans le fichier XML.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class GenerateurXML extends Donnees {

	private Document document;
	private Element racine;
	private File fichier;
	private Element lot,donneeXML;
	
	public GenerateurXML (String nomFichier) {
		this.racine = new Element("lots");
		this.document = new Document(racine, new DocType(nomFichier, "Fichiers de donn�es/generateur.dtd"));
		this.fichier = new File("Fichiers XML/" + nomFichier + ".xml");
	}
	
	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.lot = new Element("lot").setAttribute("nom", nomLot);
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		this.racine.addContent(this.lot);
		XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
        try {
            sortie.output(document, new FileWriter(fichier));
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	@Override
	public void traiter(Position position,double valeur) {
		this.donneeXML = new Element("donnee").setAttribute("id",UUID.randomUUID().toString())
											  .setAttribute("x", position.x+ "")
											  .setAttribute("y", position.y +"");
		
		Element valeurXML = new Element("valeur").setText(valeur+"");
		this.donneeXML.addContent(valeurXML);
		this.lot.addContent(donneeXML);
		super.traiter(position, valeur);
		
	}

}
