import java.util.ArrayList;
import java.util.List;

/**
 * Inverser modifie la valeur pour en faire son inverse
 * lors du traitement de ce lot.
 *
 * @author	Nathan Balech <nathan.balech.auditeur@lecnam.net>
 */

public class Inverser extends Traitement{	
	
	@Override
	public void traiter(Position position, double valeur) {
		valeur = 1 / valeur;
		super.traiter(position, valeur);
	}
	
}
