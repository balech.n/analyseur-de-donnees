import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
  * Donnees enregistre toutes les donn�es re�ues, quelque soit le lot.
  *
  * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
  */
public class Donnees extends Traitement {

	private ArrayList<Position> positions = new ArrayList<>();
	private ArrayList<Double> valeurs = new ArrayList<>();
	
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println("Donnees :");
		for(int i = 0; i < this.positions.size(); i++) {
			System.out.println("\t - Position(" + this.positions.get(i).x + "," + this.positions.get(i).y + ") -> " + this.valeurs.get(i));
		}
		System.out.println("Fin Donnees");
	}
	
	public ArrayList<Position> getPositions() {
		return positions;
	}

	public ArrayList<Double> getValeurs() {
		return valeurs;
	}

	@Override
	public void traiter(Position position, double valeur) {
		this.positions.add(position);
		this.valeurs.add(valeur);
		super.traiter(position, valeur);
	}

}
