import java.io.*;

import org.jdom2.*;
import org.jdom2.input.SAXBuilder;

import java.util.List;
import java.util.Iterator;

/** Analyser des donn�es d'un fichier, une donn�e par ligne avec 4 informations
 * s�par�es par des blancs : x, y, ordre (ignor�e), valeur.
 */
public class AnalyseurInitial {
	
	private Document document;
	private Element racine;
	
	/** Charger l'analyseur avec les donn�es du fichier "donnees.java". */
	public void traiter() {
		
		try (BufferedReader in = new BufferedReader(new FileReader("Fichiers de Donn�es/donnees.txt"))) {
			double somme = 0.0;
			String ligne = null;
			while ((ligne = in.readLine()) != null) {
				String[] mots = ligne.split("\\s+");
				assert mots.length == 4;	// 4 mots sur chaque ligne
				double valeur = Double.parseDouble(mots[3]);
				int x = Integer.parseInt(mots[0]);
				int y = Integer.parseInt(mots[1]);
				Position p = new Position(x, y);
				somme += valeur;
			}
			System.out.println(somme);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		
	}
	/*	try {
			this.lireFichier("Fichiers XML/donnees2.xml");
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.listeDonneesXML();
	}
	public void lireFichier(String nomFichier) throws JDOMException, IOException {
		SAXBuilder builder = new SAXBuilder();
		File fichierXML = new File(nomFichier);
		document = builder.build(fichierXML);
		racine = document.getRootElement();
	}
	
	public void listeDonneesXML() {
		List<Element> donnees = this.racine.getChildren("donnee");
		Iterator<Element> i = donnees.iterator();
		double somme = 0.0;
		while(i.hasNext()) {
			Element courant = (Element) i.next();

			try {
				int x = courant.getAttribute("x").getIntValue();
				int y = courant.getAttribute("y").getIntValue();
				Position p = new Position(x, y);
				somme = Double.parseDouble(courant.getChild("valeur").getText());
			} catch (DataConversionException e) {
				e.printStackTrace();
			}
		}
		System.out.println(somme);
	}*/

	public static void main(String[] args) {
		new AnalyseurInitial().traiter();
	}

}
