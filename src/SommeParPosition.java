

import java.util.*;

/**
  * SommeParPosition 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class SommeParPosition extends Traitement {
	
	private Map<Position, Double> positions = new HashMap<>();
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println("SommeParPosition " + nomLot + " :");
		for(Position p : this.positions.keySet()) {
			System.out.println("\t - " + p + " -> " + this.positions.get(p));
		}
		System.out.println("Fin SommeParPosition");
	}


	@Override
	public void traiter(Position position, double valeur) {
		if(this.positions.containsKey(position)) {
			this.positions.put(position, this.positions.get(position) + valeur);
		}else {
			this.positions.put(position, valeur);
		}
		super.traiter(position, valeur);
	}

}
